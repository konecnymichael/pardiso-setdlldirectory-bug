
program pardisosetdlldirectorybug

    use binary_files

    implicit none

    real(8), allocatable :: a(:)
    integer, allocatable :: ia(:), ja(:)
    integer :: rows, cols
    
    real(8), allocatable :: b(:,:), x(:,:)

    integer :: pt(64)
    INTEGER :: maxfct, mnum, mtype, phase, n, nrhs, error, msglvl
    INTEGER :: iparm(64)

    integer, dimension(1) :: idum

    iparm = 0
   
    error = 0 
    msglvl = 1 ! print statistical information
    mtype = -2 ! -2 == symmetric indefinite
    pt = 0 ! internal solver memory pointer
    maxfct = 1
    mnum = 1
    nrhs = 1

    call load_CSR(rows, cols, IA, JA, A, 'data/a.csr')
    
    ! matrix is square, so rows == cols
    allocate(x(rows, 1))
    allocate(b(rows, 1))
    
    phase = 13

    call PARDISO(pt, maxfct, mnum, mtype, phase, rows, a, ia, ja, idum, nrhs, iparm, msglvl, b, x, error )

    IF (error .NE. 0) THEN
        stop
    END IF

    phase = -1 ! release internal memory
    call PARDISO( pt, maxfct, mnum, mtype, phase, rows, a, ia, ja, idum, nrhs, iparm, msglvl, b, x, error )
    
    deallocate(x, b, a, ia, ja)

end program pardisosetdlldirectorybug

