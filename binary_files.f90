module binary_files
    implicit none

    interface load_vector_int
        module procedure load_vector_int_2dim, load_vector_int_1dim
    end interface

    interface load_vector_double
        module procedure load_vector_double_2dim, load_vector_double_1dim
    end interface

    interface save_vector_int
        module procedure save_vector_int_2dim, save_vector_int_1dim
    end interface

    interface save_vector_double
        module procedure save_vector_double_2dim, save_vector_double_1dim
    end interface

    contains

    subroutine load_matrix_int(A, filename)
        implicit none
        integer, dimension(:, :), intent(out), allocatable :: A
        character(len=*), intent(in) :: filename

        integer :: fid, rows, cols

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'read')
        read(fid) rows
        read(fid) cols

        allocate(A(rows, cols))
        read(fid) A
        close(fid)

    end subroutine

    subroutine load_matrix_double(A, filename)
        implicit none
        real(8), dimension(:, :), intent(out), allocatable :: A
        character(len=*), intent(in) :: filename

        integer :: fid, rows, cols

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'read')
        read(fid) rows
        read(fid) cols

        allocate(A(rows, cols))
        read(fid) A
        close(fid)

    end subroutine


    subroutine load_vector_int_2dim(v, filename)
        implicit none
        integer, dimension(:, :), intent(out), allocatable :: v
        character(len=*), intent(in) :: filename

        integer :: fid, rows, cols

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'read')
        read(fid) rows
        read(fid) cols

        if (cols /= 1) then
            close(fid)
            write (*,*) 'The binary file is a matrix'
            stop -100000
        endif

        allocate(v(rows, cols))
        read(fid) v
        close(fid)

    end subroutine

    subroutine load_vector_int_1dim(v, filename)
        implicit none
        integer, dimension(:), intent(out), allocatable :: v
        character(len=*), intent(in) :: filename

        integer :: fid, rows, cols

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'read')
        read(fid) rows
        read(fid) cols

        if (cols /= 1) then
            close(fid)
            write (*,*) 'The binary file is a matrix'
            stop -100000
        endif

        allocate(v(rows))
        read(fid) v
        close(fid)

    end subroutine

    subroutine load_vector_double_2dim(v, filename)
        implicit none
        real(8), dimension(:, :), intent(out), allocatable :: v
        character(len=*), intent(in) :: filename

        integer :: fid, rows, cols

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'read')
        read(fid) rows
        read(fid) cols

        if (cols /= 1) then
            close(fid)
            write (*,*) 'The binary file is a matrix'
            stop -100000
        endif

        allocate(v(rows, cols))
        read(fid) v
        close(fid)

    end subroutine

    subroutine load_vector_double_1dim(v, filename)
        implicit none
        real(8), dimension(:), intent(out), allocatable :: v
        character(len=*), intent(in) :: filename

        integer :: fid, rows, cols

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'read')
        read(fid) rows
        read(fid) cols

        if (cols /= 1) then
            close(fid)
            write (*,*) 'The binary file is a matrix'
            stop -100000
        endif

        allocate(v(rows))
        read(fid) v
        close(fid)

    end subroutine

    subroutine load_CSR(rows, cols, IA, JA, A, filename)
        implicit none
        integer, intent(out) :: rows
        integer, intent(out) :: cols
        integer, dimension(:), allocatable, intent(out) :: IA
        integer, dimension(:), allocatable, intent(out) :: JA
        real(8), dimension(:), allocatable, intent(out) :: A
        character(len=*), intent(in) :: filename

        integer :: fid, nnz

        fid = 1

        open(fid, file = filename, form = 'BINARY', action = 'read')
        read(fid) rows
        read(fid) cols
        read(fid) nnz

        if (allocated(IA)) deallocate(IA)
        if (allocated(JA)) deallocate(JA)
        if (allocated(A)) deallocate(A)

        allocate(IA(rows+1))
        allocate(JA(nnz))
        allocate(A(nnz))

        read(fid) IA
        read(fid) JA
        read(fid) A

        close(fid)

    end subroutine

    subroutine save_matrix_int(A, filename)
        implicit none
        integer, dimension(:, :), intent(in) :: A
        character(len=*), intent(in) :: filename

        integer :: fid

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'write')
        write(fid) size(A, 1)
        write(fid) size(A, 2)

        write(fid) A
        close(fid)

    end subroutine

    subroutine save_matrix_double(A, filename)
        implicit none
        real(8), dimension(:, :), intent(in) :: A
        character(len=*), intent(in) :: filename

        integer :: fid

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'write')
        write(fid) size(A, 1)
        write(fid) size(A, 2)

        write(fid) A
        close(fid)

    end subroutine

    subroutine save_vector_int_2dim(v, filename)
        implicit none
        integer, dimension(:, :), intent(in) :: v
        character(len=*), intent(in) :: filename

        integer :: fid

        if (size(v, 2) /= 1) then
            write (*,*) 'the vector is expected to be vertical'
            stop -100000
        endif

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'write')
        write(fid) size(v, 1)
        write(fid) int(1)

        write(fid) v
        close(fid)

    end subroutine

    subroutine save_vector_int_1dim(v, filename)
        implicit none
        integer, dimension(:), intent(in) :: v
        character(len=*), intent(in) :: filename

        integer :: fid

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'write')
        write(fid) size(v)
        write(fid) int(1)

        write(fid) v
        close(fid)

    end subroutine

    subroutine save_vector_double_2dim(v, filename)
        implicit none
        real(8), dimension(:, :), intent(in) :: v
        character(len=*), intent(in) :: filename

        integer :: fid

        if (size(v, 2) /= 1) then
            write (*,*) 'the vector is expected to be vertical'
            stop -100000
        endif

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'write')
        write(fid) size(v, 1)
        write(fid) int(1)

        write(fid) v
        close(fid)

    end subroutine

    subroutine save_vector_double_1dim(v, filename)
        implicit none
        real(8), dimension(:), intent(in) :: v
        character(len=*), intent(in) :: filename

        integer :: fid

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'write')
        write(fid) size(v)
        write(fid) int(1)

        write(fid) v
        close(fid)

    end subroutine

    subroutine save_CSR(rows, cols, IA, JA, A, filename)
        implicit none
        integer, intent(in) :: rows
        integer, intent(in) :: cols
        integer, dimension(:), intent(in) :: IA
        integer, dimension(:), intent(in) :: JA
        real(8), dimension(:), intent(in) :: A
        character(len=*), intent(in) :: filename

        integer :: fid, nnz

        nnz = size(A)

        fid = 1
        open(fid, file = filename, form = 'BINARY', action = 'write')
        write(fid) rows
        write(fid) cols
        write(fid) nnz

        write(fid) IA
        write(fid) JA
        write(fid) A

        close(fid)

    end subroutine

end module

