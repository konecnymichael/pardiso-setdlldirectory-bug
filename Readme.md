When pardiso is run with `msglvl = 1`, for some reason it calls SetDllDirectoryA() from kernel32 and changes the way DLLs are loaded for the rest of the process execution.

![call stack](images/call_stack.png)

This can be reproduced by setting a breakpoint to `kernel32!SetDllDirectoryA` and running the program in the pardiso-setdlldirectory-bug project.

![breakpoints](images/breakpoints.png)

In a different project, I was able to find out SetDllDirectory was being called from MKL's mkl_serv_print() function.

![name of troublesome MKL function](images/mkl_custom_dll.png)

This happens on both oneAPI toolkit versions 2021.3.0 and 2021.4.0 (currently latest).

This behaviour is very annoying because:

- it is unexpected
- it started happening after an update from Intel 19.0
- it broke [delay loading](https://docs.microsoft.com/en-us/cpp/build/reference/linker-support-for-delay-loaded-dlls?view=msvc-170) of a library which was called later in a program, which took me two weeks to debug
- it seems unnecessary

I consider this to be a bug as this call doesn't seem to have been present in the previous versions of MKL and doesn't seem necessary.

I would appreciate if Intel could do either of:

- remove the SetDllDirectory call
- at least reset the DLL loading mode to previous settings as not to mess up the behaviour of the process which calls it

